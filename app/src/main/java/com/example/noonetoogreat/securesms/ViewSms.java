package com.example.noonetoogreat.securesms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ViewSms extends AppCompatActivity {
    public String one,two,three;
    static String PASSKEY, TEST_STRING, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_sms);

        Intent i = getIntent();
        one = i.getExtras().getString("1");
        two = i.getExtras().getString("2");
        three= i.getExtras().getString("3");

        TextView sender_number_view = (TextView)findViewById(R.id.sender_number);
        TextView encrypted_message_view = (TextView)findViewById(R.id.encrypted_message);
        TextView decrypted_message_view = (TextView)findViewById(R.id.decrypted_message);
        sender_number_view.setText(one);
        encrypted_message_view.setText(two);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String senderNumberPref = sharedPref.getString("pref_sender_phone", null);
        //PASSKEY= senderNumberPref;
        if(three.equals("inbox")){

            PASSKEY= senderNumberPref;
        }
        else{
            TextView textView_view = (TextView)findViewById(R.id.textView);
            textView_view.setText("Receiver");
            PASSKEY= one;
        }
        TEST_STRING = two;
        try {
            Crypto crypto = new Crypto(PASSKEY);
            decrypted_message_view.setText(crypto.decrypt(TEST_STRING));
        }
        catch (Exception e) {
            result += "Error: " + e.getMessage() + "\n";
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_sms, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
