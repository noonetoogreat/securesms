package com.example.noonetoogreat.securesms;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnSent, btnInbox, btnDraft;
    TextView lblMsg, lblNo;
    ListView lvMsg;
    SimpleAdapter listAdapter;

    // Cursor Adapter
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean initializePref = sharedPref.getBoolean("pref_initialize", true);

        if(initializePref) {
            Intent splashIntent = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(splashIntent);
        }

        btnInbox = (Button) findViewById(R.id.btnInbox);
        btnInbox.setOnClickListener(this);

        btnSent = (Button)findViewById(R.id.btnSentBox);
        btnSent.setOnClickListener(this);

        btnDraft = (Button)findViewById(R.id.btnDraft);
        btnDraft.setOnClickListener(this);

        lvMsg = (ListView) findViewById(R.id.lvMsg);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {

        if (v == btnInbox) {
            Uri inboxURI = Telephony.Sms.Inbox.CONTENT_URI;
            // List required columns
            String[] reqCols = new String[] { "_id", "address", "body" };

            List<HashMap<String, String>> inboxMessages = new ArrayList<HashMap<String, String>>();
            // Get Content Resolver object, which will deal with Content Provider
            ContentResolver cr = getContentResolver();

            // Fetch Inbox SMS Message from Built-in Content Provider
            final String sa1 = "}@@!%";
            final Cursor c = cr.query(inboxURI, reqCols, Telephony.Sms.BODY+ " LIKE ?",new String[] { sa1 }, null);

            // Attached Cursor with adapter and display in listview
            //adapter = new SimpleCursorAdapter(this, R.layout.row_sms, c, new String[] { "body", "address" }, new int[] {R.id.lblMsg, R.id.lblNumber });
            //lvMsg.setAdapter(adapter);

            c.moveToPosition(-1);
            while (c.moveToNext()) {
                HashMap<String, String> labels = new HashMap<String, String>();
                labels.put("id", c.getString(0));
                labels.put("sender", c.getString(1));
                labels.put("enc_message", c.getString(2).substring(4,34) + "...");
                String encryptedMessage = c.getString(2).substring(4);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                String senderNumberPref = sharedPref.getString("pref_sender_phone", null);
                String PASSKEY = senderNumberPref;
                String decyptedMessage = new String();
                Crypto crypto = new Crypto(PASSKEY);
                decyptedMessage = crypto.decrypt(encryptedMessage);
                if(decyptedMessage.length() > 20) {
                    labels.put("dec_message", decyptedMessage.substring(0,20) + "...");
                }
                else {
                    labels.put("dec_message", decyptedMessage);
                }

                inboxMessages.add(labels);

            }

            listAdapter = new SimpleAdapter(MainActivity.this, inboxMessages, R.layout.row_sms_complete, new String[] {"sender", "enc_message", "dec_message"}, new int[] {R.id.sender_number_home, R.id.enc_message, R.id.dec_message} );
            lvMsg.setAdapter(listAdapter);
            lvMsg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    c.moveToPosition(i);
                    Intent intent = new Intent(MainActivity.this, ViewSms.class);
                    intent.putExtra("0", c.getString(0));
                    intent.putExtra("1", c.getString(1));
                    intent.putExtra("2", c.getString(2).substring(4));
                    intent.putExtra("3", "inbox");
                    startActivity(intent);
                }
            });
        }

        if(v==btnSent) {
            // Create Sent box URI
            Uri sentURI = Telephony.Sms.Sent.CONTENT_URI;

            // List required columns
            String[] reqCols = new String[] { "_id", "address", "body" };

            List<HashMap<String, String>> outboxMessages = new ArrayList<HashMap<String, String>>();

            // Get Content Resolver object, which will deal with Content Provider
            ContentResolver cr = getContentResolver();

            // Fetch Sent SMS Message from Built-in Content Provider
            final String sa1 = "}@@!%";
            final Cursor c = cr.query(sentURI, reqCols, Telephony.Sms.BODY+ " LIKE ?",new String[] { sa1 }, null);

            c.moveToPosition(-1);
            while (c.moveToNext()) {
                HashMap<String, String> labels = new HashMap<String, String>();
                labels.put("id", c.getString(0));
                labels.put("sender", c.getString(1));
                labels.put("enc_message", c.getString(2).substring(4,34) + "...");
                String encryptedMessage = c.getString(2).substring(4);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                String senderNumberPref = sharedPref.getString("pref_sender_phone", null);
                String PASSKEY = c.getString(1);
                String decyptedMessage = new String();
                Crypto crypto = new Crypto(PASSKEY);
                decyptedMessage = crypto.decrypt(encryptedMessage);
                if(decyptedMessage.length() > 20) {
                    labels.put("dec_message", decyptedMessage.substring(0,20) + "...");
                }
                else {
                    labels.put("dec_message", decyptedMessage);
                }

                outboxMessages.add(labels);

            }

            listAdapter = new SimpleAdapter(MainActivity.this, outboxMessages, R.layout.row_sms_complete, new String[] {"sender", "enc_message", "dec_message"}, new int[] {R.id.sender_number_home, R.id.enc_message, R.id.dec_message} );
            lvMsg.setAdapter(listAdapter);
            lvMsg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    c.moveToPosition(i);
                    Intent intent = new Intent(MainActivity.this, ViewSms.class);
                    intent.putExtra("0", c.getString(0));
                    intent.putExtra("1", c.getString(1));
                    intent.putExtra("2", c.getString(2).substring(4));
                    intent.putExtra("3", "outbox");
                    startActivity(intent);
                }
            });


        }

        if(v == btnDraft)
        {
            Intent i = new Intent(MainActivity.this, SendSMS.class);
            startActivity(i);
        }

    }
}
