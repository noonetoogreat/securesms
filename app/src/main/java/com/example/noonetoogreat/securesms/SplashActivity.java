package com.example.noonetoogreat.securesms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    EditText decrypt_key;
    Button submit;
    public static String PHONE_NUMBER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        decrypt_key = (EditText)findViewById(R.id.et_dekey);
        //Intent i = new Intent(SplashActivity.this,MainActivity.class);
        //startActivity(i);
        submit = (Button)findViewById(R.id.b_submit);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b_submit:
                PHONE_NUMBER = decrypt_key.getText().toString();

                String regexStr = "^[0-9]{10}$";


                if(PHONE_NUMBER.length()<10 || PHONE_NUMBER.matches(regexStr)==false  ) {
                    Toast.makeText(SplashActivity.this,"Please enter a valid phone number", Toast.LENGTH_SHORT).show();
                }
                else {
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                    sharedPref.edit().putBoolean("pref_initialize", false).commit();
                    sharedPref.edit().putString("pref_sender_phone", PHONE_NUMBER).commit();

                    Intent i = new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(i);
                }


                break;
        }
    }
}

