package com.example.noonetoogreat.securesms;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SendSMS extends AppCompatActivity implements View.OnClickListener{
    EditText txtNumber, txtMessage;
    Button btnSend;
    String number,message,message1,out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        txtNumber = (EditText) findViewById(R.id.txtNumber);
        txtMessage = (EditText) findViewById(R.id.txtMesssage);
        btnSend = (Button) findViewById(R.id.btnSMS);
        btnSend.setOnClickListener(this);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String senderNumberPref = sharedPref.getString("pref_sender_phone", null);
        
        TextView sender_number_text_view = (TextView)findViewById(R.id.sender_number_send);
        sender_number_text_view.setText(senderNumberPref);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int i=0,j=0,k=0;
        if (v == btnSend) {

            number = txtNumber.getText().toString();
            String regexStr = "^[0-9]{10}$";


            if(number.length()<10 || number.matches(regexStr)==false  ) {
                Toast.makeText(SendSMS.this,"Please enter a valid phone number", Toast.LENGTH_SHORT).show();
            }
            else{
                message =  txtMessage.getText().toString();
                //message="";
                /*while (k<127){
                    message=message+"a";
                    Crypto crypto = new Crypto(number);
                    k=k+1;
                }*/
                if (message.length()<=60){
                    try {
                        Crypto crypto = new Crypto(number);
                        out=crypto.encrypt(message);
                        String tag = "watiswrng";
                        /*Log.v(tag, out);
                        Log.v(tag,String.valueOf(out.length()));
                        Log.v(tag, message);
                        Log.v(tag,String.valueOf(message.length()));*/
                        out = "}@@!"+ out;
                        //Log.v(tag, out);
                        //Log.v(tag,String.valueOf(out.length()));
                    } catch (Exception e) {
                        out += "Error: " + e.getMessage() + "\n";
                        e.printStackTrace();
                    }
                    // Initialize SmsManager Object
                    SmsManager smsManager = SmsManager.getDefault();


                    // Send Message using method of SmsManager object
                    smsManager.sendTextMessage(txtNumber.getText().toString(), null, out, null, null);

                    Toast.makeText(this, "Message sent successfully", Toast.LENGTH_LONG).show();
                    Intent x = new Intent(SendSMS.this, MainActivity.class);
                    startActivity(x);

                }
                else{
                    i=message.length()/60+1;
                    while (i>0){
                        try {
                            if (i==1){
                                message1=message.substring(j);
                            }
                            else {
                                message1 = message.substring(j, j + 60);
                            }
                            Crypto crypto = new Crypto(number);
                            out=crypto.encrypt(message1);
                            String tag = "watiswrng";
                            /*Log.v(tag, out);
                            Log.v(tag,String.valueOf(out.length()));
                            Log.v(tag, message1);
                            Log.v(tag,String.valueOf(message1.length()));*/
                            out = "}@@!"+ out;
                            //Log.v(tag, out);
                            //Log.v(tag,String.valueOf(out.length()));
                        } catch (Exception e) {
                            out += "Error: " + e.getMessage() + "\n";
                            e.printStackTrace();
                        }
                        // Initialize SmsManager Object
                        SmsManager smsManager = SmsManager.getDefault();


                        // Send Message using method of SmsManager object
                        smsManager.sendTextMessage(txtNumber.getText().toString(), null, out, null, null);


                        j=j+60;
                        i=i-1;
                    }
                    Toast.makeText(this, "Messages sent successfully", Toast.LENGTH_LONG).show();
                    Intent x = new Intent(SendSMS.this, MainActivity.class);
                    startActivity(x);

                }
            }



        }


    }
}
